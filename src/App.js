import React, { Component } from 'react';
import PostList from './components/PostList';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react'
import { store, persistor } from './redux/store.js';

class App extends Component {
  render() {
    return (
      <Provider store = {store}>
        <PersistGate loading={null} persistor = {persistor}>
          <PostList/>
        </PersistGate>
      </Provider>
    );
  }
}

export default App;