import React, { Component } from 'react';
import PropTypes from 'prop-types';
import TextField from '@material-ui/core/TextField';
import * as actions from '../redux/post/actions.js';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';

class Post extends Component{
  // 이게 하나의 sublist가 될듯
  // state = {
  //   selectedIndex: 1,
  // };
  // handleListItemClick = (event, index) => {
  //   this.setState({ selectedIndex: index });
  // };
  // props.id로 redux의 selected를 onclick에 바꿈다.

  render(){
    const leftPadding = 25;
    console.warn(this.props.level, this.props.posts);
    if(this.props.posts==null) return (<h1>gg</h1>);
    const currentPosts = this.props.posts.filter((childpost) => childpost.rtpn == this.props.postId)
    return(
      <List>
          {
            currentPosts.map((post, postId) => (
              <div>
                <ListItem
                  button
                  selected={this.props.selected === post.postId}
                  onClick = {event => this.props.setSelected(post.postId)}
                >
                  <ListItemText primary={post.content}/>
                </ListItem>
                <div style={{ paddingLeft: leftPadding }}>
                  <Post
                    setSelected={this.props.setSelected}
                    posts={this.props.posts}
                    selected={this.props.selected}
                    level={this.props.level + 1}
                    postId={post.postId}
                  />
                </div>
              </div>
            )
          )
        }
      </List>
      
    );
  }
}

class PostList_ extends Component{

  handleChangeContent = content => e => {
    this.setState( 
      {
        post: {
          content : e.target.value,
          postId : this.state.post.postId,
          rtpn : this.state.post.rtpn,
        }
      }
    );
  }

  constructor(props){
    super(props);
    this.state = {
      post : {
        postId : "0",
        content : "ㅎㅅㅎ",
      }
    }
  }

  render(){

    console.warn(this.props.posts)
    return (
      <div>
        <>
        <div>
          <TextField
            label="Add Reply"
            floatingLabelText="Add Reply"
            onChange={this.handleChangeContent('content')}
          />
          <Button 
            variant="contained" 
            color="primary" 
            onClick={() => this.props.onAdd(this.state.post.content)}>
            Add
          </Button>
          <Button 
            variant="contained" 
            color="secondary" 
            onClick={() => this.props.onReset()}>
            Reset Selected
          </Button>
        </div>
        <div style={{paddingLeft:15}}>
          <Post
            setSelected={this.props.setSelected}
            posts={this.props.posts}
            postId={0}
            selected={this.props.selected}
            level={0}
          />
        </div>
        

        </>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  posts: state.managePosts.posts,
  selected: state.managePosts.selected,
});

const mapDispatchToProps = (dispatch) => ({
  onAdd : (content) => {dispatch(actions.addPost(content))},
  onReset : () => {dispatch(actions.resetSelected())},
  setSelected : (selected) => {dispatch(actions.setSelected(selected))},
})

const PostList =  connect(mapStateToProps, mapDispatchToProps)(PostList_);

export default PostList;
