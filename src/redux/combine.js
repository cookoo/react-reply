import { combineReducers } from 'redux';
import managePosts from './post/reducer.js';

const combineReducer = combineReducers({
    managePosts
});

export default combineReducer;