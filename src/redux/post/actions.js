import * as types from './types.js';

export function addPost(content){
  return{
      type : types.ADD_POST,
      content,
  }
}

export function resetSelected(){
  return{
      type : types.RESET_SELECTED,
    
  }
}

export function setSelected(selected){
  return{
    type : types.SET_SELECTED,
    selected,
  }

}