import * as types from './types.js';

const initialState = {
    selected: 0,
    posts: [
      {
        postId : 1,
        rtpn : 0,
        content : "HI!",
      },
      {
        postId : 2,
        rtpn : 0,
        content : "HI!",
      },
      {
        postId : 3,
        rtpn : 2,
        content : "HI!",
      },
      {
        postId : 4,
        rtpn : 2,
        content : "HI!",
      },
      {
        postId : 5,
        rtpn : 4,
        content : "HI!",
      },
      {
        postId : 6,
        rtpn : 0,
        content : "HI!",
      },
      {
        postId : 7,
        rtpn : 6,
        content : "HI!",
      },
      {
        postId : 8,
        rtpn : 7,
        content : "HI!",
      },
    ],
};

function managePosts (state = initialState, action) {
    switch(action.type) {
        case types.ADD_POST:
          return { 
            ...state,  
            posts: [...state.posts, {postId:(state.posts.length+1), rtpn:state.selected, content:action.content}],
          };
        case types.RESET_SELECTED:
          return { 
            ...state,  
            selected : 0,
          };    
        case types.SET_SELECTED:
          return {
            ...state,
            selected : action.selected,
          }
        default:
            return state;
    }
}

export default managePosts;
