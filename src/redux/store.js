import { persistReducer, persistStore } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import combineReducer from './combine.js'
import { createStore } from 'redux';

const persistConfig = {
  key: "root",
  storage: storage,
};

export const store = createStore(
  persistReducer(persistConfig, combineReducer),
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

export const persistor = persistStore(store);